 /*
    Create functions which can manipulate our arrays.
*/

alert ("Welcome to the site!" + " Please register your name. ");
let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];
let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.



*/
    
      
/* 
function register(newUser){
    if(newUser == "Conan O'Brien"){ 
    registeredUsers[registeredUsers.length] = newUser;newUser.toLowerCase(); 
    let newElement = registeredUsers[registeredUsers.length];
    alert(message1); 


}
    else{
    let message2 = "Registration failed. Username already exists!";
    alert(message2);
}
};

*/
/*
function register(newuser){
    if(newuser !== registeredUsers){
       registeredUsers[registeredUsers.length] = newuser
       let newElement = registeredUsers.includes(newuser);
       alert("Thank you for registering!");
}
else {
    registeredUsers[registeredUsers.length] = newuser
    let newElement = registeredUsers.includes(newuser);
    alert ("Registration failed. Username already exists!");
}
};
    
*/

function register(newuser){

    if (registeredUsers.includes(newuser)){ 
        alert("Registration failed. Username already exists!");
    }
    else{
        registeredUsers.push(newuser);
        alert ("Thank you for registering!");  
    }
};


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/    

//addFriend   


function addFriend(friend){

    if (registeredUsers.includes(friend)){ 
        friendsList.push(friend);
        console.log(friendsList);
    }
    else{
        alert("User not found."); 
    }
};


/*

function addFriend(friend){
    if(friend == "James Jeffries"){
    friendsList[friendsList.length] = friend
    let newFriend = friendsList[friendsList.length];
    console.log(friendsList);
}

else if (friend == "Gunther Smith"){
    friendsList[friendsList.length] = friend
    let newFriend = friendsList[friendsList.length];
    console.log(friendsList);

}

else if (friend == "Macie West"){
    friendsList[friendsList.length] = friend
    let newFriend = friendsList[friendsList.length];
    console.log(friendsList);

}

else if (friend == "Michelle Queen"){
    friendsList[friendsList.length] = friend
    let newFriend = friendsList[friendsList.length];
    console.log(friendsList);

}

else if (friend == "Shane Miguelito"){
    friendsList[friendsList.length] = friend
    let newFriend = friendsList[friendsList.length];
    console.log(friendsList);

}

else if (friend == "Fernando Dela Cruz"){
    friendsList[friendsList.length] = friend
    let newFriend = friendsList[friendsList.length];
    console.log(friendsList); 

}

else if (friend == "Akiko Yukihime"){
    friendsList[friendsList.length] = friend
    let newFriend = friendsList[friendsList.length];
    console.log(friendsList); 

}
else {
    alert("User not found.");
}
};

*/


function displayNumberOfFriends(){
    let uniqueFriends = [...new Set(friendsList)];
    let newFriend = uniqueFriends.length; 
    if (friendsList.length == 0){
        alert("You currently have 0 friends. Add one first.");
    }
    else {
        alert("You currently have " + newFriend + " friend(s).");
        console.log(friendsList[0]);
        console.log(friendsList[1]);
    }
};

// deleteFriend 

function deleteFriend(friend){ 
  let i = friendsList.length;
  if(i > 0){
    friendsList.pop();
    console.log(friendsList);
}
else {
    alert("You currently have 0 friends. Add one first.");
}
};


/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/
    


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/






